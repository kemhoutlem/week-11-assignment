<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'role' => 'admin',
            'first_name' => 'Kem',
            'last_name' => 'Hout',
            'email' => 'admin@gmail.com',
            'password' => '1234'
        ]);
    }
}
