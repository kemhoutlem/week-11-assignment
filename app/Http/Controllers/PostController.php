<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Category;

class PostController extends Controller
{
    public function index()
    {
        $posts = Post::get();
        return view('posts.index', [
            'posts' => $posts
        ]);
    }

    public function create()
    {
        if (Auth::user()->role !== 'admin') {
            return redirect(route('home'));
        }
        $categories = Category::get();
        return view('posts.create', [
            'categories' => $categories
        ]);
    }

    public function store(Request $request)
    {
        if (Auth::user()->role !== 'admin') {
            return redirect(route('home'));
        }
        $input = $request->all();
        $input['user_id'] = Auth::id();
        Post::create($input);
        return redirect(route('posts.index'));
    }

    public function edit(Post $post)
    {
        if (Auth::user()->role !== 'admin') {
            return redirect(route('home'));
        }
        $categories = Category::pluck('name', 'id');
        return view('posts.edit', [
            'categories' => $categories,
            'post' => $post
        ]);
    }

    public function update(Request $request, Post $post)
    {
        if (Auth::user()->role !== 'admin') {
            return redirect(route('home'));
        }
        $post->title = $request->get('title');
        $post->content = $request->get('content');
        $post->save();
        return redirect(route('posts.index'));
    }

    public function destroy(Post $post)
    {
        if (Auth::user()->role !== 'admin') {
            return redirect(route('home'));
        }
        $post->delete();
        return redirect(route('posts.index'));
    }
}
