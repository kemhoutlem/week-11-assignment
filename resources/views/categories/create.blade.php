@extends('layouts.app')

@section('content')
    <!-- Content Row   -->
    <div class="row"> 
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">New Category</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <form action = {{ route('categories.store') }} method="POST" class="user">
                    @csrf
                    <div class="form-group row">
                        <div class="col-sm-12">
                            <input type="text" required class="form-control form-control-user" name="name" placeholder="Category Name">
                        </div>
                    </div>
                    <button class="btn btn-primary btn-user btn-block">
                        Save
                    </button>
                </form>
            </div>
        </div>
        </div>
    </div>    
@endsection